#!/usr/bin/python3

import sys


FIG_NUM_SPACE = -1  # Internal number representing a space used by _fig_core()


def _fig_core(list_l1, n, num_space=FIG_NUM_SPACE):

    #
    # step 1 - create a strentched list having num_space
    #   e.g. IN= [           1,            2,            3           ]
    #       OUT= [num_space, 1, num_space, 2, num_space, 3, num_space]
    #
    list_l1_stretched = list()
    for num in list_l1:
        list_l1_stretched.append(num_space)
        list_l1_stretched.append(num)
    list_l1_stretched.append(num_space)

    #
    # step 2 - create a list to save the index of all num_space
    #   e.g. IN= [num_space, 1, num_space, 2, num_space, 3, num_space]
    #       OUT= [        0,            2,            4,            6]
    list_idx = list()
    idx = 0
    for num in list_l1_stretched:
        if num == num_space:
            list_idx.append(idx)
        idx += 1

    #
    # step 3 - create middle L2 list by insterting n at the index of num_space
    #   e.g.  L1=  [num_space, 1, num_space, 2, num_space, 3, num_space]
    #        IDX=  [        0,            2,            4,            6]
    #          n=  4
    #           +  ---------- --  --------- --  --------- --         ---- ---
    #           +  |        0  1          2  3          4  5          6 | IDX
    #           +  ---------- --  --------- --  --------- --         ---- ---
    #        OUT= [[        4, 1, num_space, 2, num_space, 3, num_space],
    #              [num_space, 1,         4, 2, num_space, 3, num_space],
    #              [num_space, 1, num_space, 2,         4, 3, num_space],
    #              [num_space, 1, num_space, 2, num_space, 3,         4]]
    #
    list_l2_mid = list()
    for idx in list_idx:
        list_l1_copy = [num for num in list_l1_stretched]
        list_l1_copy[idx] = n
        list_l2_mid.append(list_l1_copy)

    #
    # step 4 - remove num_space from list_l2_mid one by one
    #   e.g.  IN= [[        4, 1, num_space, 2, num_space, 3, num_space],
    #              [num_space, 1,         4, 2, num_space, 3, num_space],
    #              [num_space, 1, num_space, 2,         4, 3, num_space],
    #              [num_space, 1, num_space, 2, num_space, 3,         4]]
    #        OUT= [[        4, 1,            2,            3           ],
    #              [           1,         4, 2,            3           ],
    #              [           1,            2,         4, 3           ],
    #              [           1,            2,            3,         4]]
    #
    out = list()
    for list_l1 in list_l2_mid:
        list_l1_shrinked = [num for num in list_l1 if num != num_space]
        out.append(list_l1_shrinked)
    return out


def _get_full_perm_byfig(list_l2, n):
    out = list()
    for list_l1 in list_l2:
        out.extend(_fig_core(list_l1, n))
    return out


def get_full_perm(n):
    if n == 1:
        return [[1]]
    else:
        return _get_full_perm_byfig(get_full_perm(n - 1), n)


def cal_one(back_list, front_list):
    out = list()
    idx = 0
    for num in back_list:
        num1 = num
        num2 = front_list[idx]
        idx += 1
        out.append(abs(num1 - num2))
    return out


def cal(list_l2, n):
    front_list = [num + 1 for num in range(n)]
    out = list()
    for list_l1 in list_l2:
        cell = dict()
        cell['back'] = list_l1
        cell['front'] = front_list
        cell['ABS'] = cal_one(list_l1, front_list)
        out.append(cell)
    return out


def has_duplicated_num(list_num):
    if len(list_num) == 1:
        return True
    #
    # XXX: we can use sorted(list) to identify there are duplicated nums too
    #      e.g.
    #      >>> l = [1, 2, 2, 1, 3]
    #      >>> sorted([1, 2, 2, 1, 3])
    #      [1, 1, 2, 2, 3]
    #
    set_num = set(list_num)
    if len(set_num) != len(list_num):
        return True
    return False


def verify(list_map):
    cnt = 0
    for xmap in list_map:
        if has_duplicated_num(xmap['ABS']):
            cnt += 1
        else:
            if len(xmap['ABS']) > 1:
                print('+OK: exception found #', xmap)

    if cnt == len(list_map):
            return True
    return False


def main(argc, argv):
    if argc != 2:
        print('Usage: %s <N> # {1 .. 9}' % argv[0], file=sys.stderr)
        return 1

    n = int(argv[1])
    if n <= 0:
        raise Exception("%d is too small, not supported!" % n)
    if n >= 10:
        raise Exception("%d is too big, not supported!" % n)

    fp_out = get_full_perm(n)
    for e in fp_out:
        print(e, file=sys.stderr)
    print('-' * 80, file=sys.stderr)

    cal_out = cal(fp_out, n)
    for e in cal_out:
        print(e, file=sys.stderr)
    print('-' * 80, file=sys.stderr)

    if verify(cal_out):
        print("%d: TRUE" % n)
        return 0
    else:
        print("%d: FALSE" % n)
        return 1


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
