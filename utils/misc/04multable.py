#!/usr/bin/python3
"""
Create 59 x 59 multiplication table for ancient Babylonian

By Huanian Li <e21920076@126.com>
On 2021/12/21
"""

import sys
import getopt


BABYLON_NS_KEY = 'K'
BABYLON_NS_FISH_BONE = 'B'
# XXX: Ancient Babylonian doesn't have number zero (0), so we use 'Z' instead
BABYLON_NS_ZERO = 'Z'


def dec2basen(n, basen=2):
    """ Convert decimal number to base-N number """
    m = n
    k = 0
    while True:
        cnt = m // basen
        if cnt == 0:
            break
        m = cnt
        k += 1

    m = n
    out = []
    i = k
    while i >= 0:
        x = m // (basen ** i)
        out.append(x)

        m -= x * (basen ** i)
        i -= 1

    return out


def basen2dec(l, basen=2):
    """ Convert base-N number to decimal number """
    n = 0
    k = len(l) - 1
    for i in l:
        n += i * (basen ** k)
        k -= 1
    return n


def dectobase60(n):
    """ Convert decimal number to base-60 number """
    return dec2basen(n, 60)


def base60todec(l):
    """ Convert base-60 number to decimal number """
    return basen2dec(l, 60)


def create_ancient_babylonian_num_sym_tbl():
    sym_table = []
    for i in range(1, 60, 1):
        n = i
        na = n // 10  # get the number of keys while key = 1
        nb = n % 10   # get the number of fish bones while fish bone = 10
        sym = BABYLON_NS_FISH_BONE * na + BABYLON_NS_KEY * nb
        sym_table.append({'b10': n, 'b60': sym})
    return sym_table


def get_sym_bydec(dec, sym_tbl):
    for sym in sym_tbl:
        if dec == sym['b10']:
            return sym['b60']
    return BABYLON_NS_ZERO


def get_sym_byb60(l60n, sym_tbl):
    out = []
    for dec in l60n:
        out.append(get_sym_bydec(dec, sym_tbl))
    return ','.join(out)


def create_mul_tbl(sym_tbl, n=59):
    mul_tbl = []
    for i in range(n):
        a = i + 1
        col = []
        for j in range(n):
            b = j + 1
            if b < a:
                continue
            #
            # MULTIPLICAND x MULTIPLIER = PRODUCT
            #     a        x      b     = c
            #
            c = a * b
            la60 = dectobase60(a)
            lb60 = dectobase60(b)
            lc60 = dectobase60(c)
            d_cell = {}
            d_cell['a'] = {}
            d_cell['b'] = {}
            d_cell['c'] = {}
            d_cell['a']['desc'] = 'multiplicand = a'
            d_cell['a']['b10'] = a
            d_cell['a']['s60'] = get_sym_byb60(la60, sym_tbl)
            d_cell['a']['l60'] = la60
            d_cell['b']['desc'] = 'multiplier = b'
            d_cell['b']['b10'] = b
            d_cell['b']['s60'] = get_sym_byb60(lb60, sym_tbl)
            d_cell['b']['l60'] = lb60
            d_cell['c']['desc'] = 'product = a x b = c'
            d_cell['c']['b10'] = c
            d_cell['c']['s60'] = get_sym_byb60(lc60, sym_tbl)
            d_cell['c']['l60'] = lc60
            col.append(d_cell)
        mul_tbl.append(col)
    return mul_tbl


def output_text(mul_tbl, verbose=False):
    for col in mul_tbl:
        for row in col:
            if verbose:
                if len(row['c']['l60']) == 2:
                    x1 = row['c']['l60'][0]
                    x2 = row['c']['l60'][1]
                else:
                    x1 = 0
                    x2 = row['c']['l60'][0]
                print('#\t%2d x %2d = %d = %2d x 60 + %2d' %
                      (row['a']['b10'], row['b']['b10'], row['c']['b10'],
                       x1, x2))
            print('%s\tx\t%s\t= %s' %
                  (row['a']['s60'], row['b']['s60'], row['c']['s60']))
        print()


def output_md(mul_tbl, output_file, verbose=False):
    pass


def new_argv(argv0, rargv):
    argv = []
    argv.append(argv0)
    argv.extend(rargv)
    return argv


def usage(argv0):
    print("Usage: %s [-v] [-m] [-o output.md] <N>" % argv0, file=sys.stderr)
    print("e.g.", file=sys.stderr)
    print("       %s 9" % argv0, file=sys.stderr)
    print("       %s -v 9" % argv0, file=sys.stderr)
    print("       %s -m -o foo.md 59" % argv0, file=sys.stderr)


def main(argc, argv):
    md_flag = False
    output_file = None
    verbose = False

    options, rargv = getopt.getopt(argv[1:],
                                   ':mo:vh',
                                   ['md', 'output=', 'verbose', 'help'])
    for opt, arg in options:
        if opt in ('-m', '--md'):
            md_flag = True
        elif opt in ('-o', '--output'):
            output_file = arg
        elif opt in ('-v', '--verbose'):
            verbose = True
        else:
            usage(argv[0])
            return 1

    argv = new_argv(argv[0], rargv)
    argc = len(argv)
    if argc < 2:
        usage(argv[0])
        return 1

    n = int(argv[1])
    sym_tbl = create_ancient_babylonian_num_sym_tbl()
    mul_tbl = create_mul_tbl(sym_tbl, n)

    if md_flag:
        output_md(mul_tbl, output_file, verbose)
    else:
        output_text(mul_tbl, verbose)

    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
