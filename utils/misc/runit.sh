#!/bin/bash
#
# Run the utility to get the multiplication table for ancient Babylonian
#
# By Huanian Li <e21920076@126.com>
# On 2021/12/21
#

FILE=$(readlink -f $BASH_SOURCE)
NAME=$(basename $FILE)
CDIR=$(dirname $FILE)
TMPDIR=${TMPDIR:-"/tmp"}

n=${1:-"59"}
util=$CDIR/04multable
$util $n | egrep -v '^#' | egrep -v '\--' | \
    expand -t 4 | tr -s ' ' ':' | sed 's/^/    /g' | lsgen
