#!/usr/bin/python3
""" Translate a decimal number to symbols of the ancient Babylonian """

import sys


BABYLON_NS_KEY = 'K'
BABYLON_NS_FISH_BONE = 'B'
# XXX: Ancient Babylonian doesn't have number zero (0), so we use 'Z' instead
BABYLON_NS_ZERO = 'Z'


def create_ancient_babylonian_num_sym_tbl():
    sym_table = []
    for i in range(1, 60, 1):
        n = i
        na = n // 10  # get the number of keys while key = 1
        nb = n % 10   # get the number of fish bones while fish bone = 10
        sym = BABYLON_NS_FISH_BONE * na + BABYLON_NS_KEY * nb
        sym_table.append({'b10': n, 'b60': sym})
    return sym_table


def dec2basen(n, basen=2):
    """ Convert decimal number to base-N number """
    m = n
    k = 0
    while True:
        cnt = m // basen
        if cnt == 0:
            break
        m = cnt
        k += 1

    m = n
    out = []
    i = k
    while i >= 0:
        x = m // (basen ** i)
        out.append(x)

        m -= x * (basen ** i)
        i -= 1

    return out


def basen2dec(l, basen=2):
    """ Convert base-N number to decimal number """
    n = 0
    k = len(l) - 1
    for i in l:
        n += i * (basen ** k)
        k -= 1
    return n


def dectobase60(n):
    """ Convert decimal number to base-60 number """
    return dec2basen(n, 60)


def base60todec(l):
    """ Convert base-60 number to decimal number """
    return basen2dec(l, 60)


def get_sym_bydec(dec, sym_tbl):
    for sym in sym_tbl:
        if dec == sym['b10']:
            return sym['b60']
    return BABYLON_NS_ZERO


def get_sym_byb60(l60n, sym_tbl):
    out = []
    for dec in l60n:
        out.append(get_sym_bydec(dec, sym_tbl))
    return ','.join(out)


def main(argc, argv):
    if argc != 2:
        print("Usage: %s <N>" % argv[0], file=sys.stderr)
        print("e.g.", file=sys.stderr)
        print("       %s 0" % argv[0], file=sys.stderr)
        print("       %s 1" % argv[0], file=sys.stderr)
        print("       %s 59" % argv[0], file=sys.stderr)
        print("       %s 61" % argv[0], file=sys.stderr)
        print("       %s 3611" % argv[0], file=sys.stderr)
        return 1

    b10n = int(argv[1])
    sym_tbl = create_ancient_babylonian_num_sym_tbl()
    l60n = dectobase60(b10n)
    print("%d\t: %s ==> %s" % (b10n, l60n, get_sym_byb60(l60n, sym_tbl)))
    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
