#!/usr/bin/python3
""" Convert decimal number to base-N number """

import sys


def dec2basen(n, basen=2):
    """ Convert decimal number to base-N number """
    m = n
    k = 0
    while True:
        cnt = m // basen
        if cnt == 0:
            break
        m = cnt
        k += 1

    m = n
    out = []
    i = k
    while i >= 0:
        x = m // (basen ** i)
        out.append(x)

        m -= x * (basen ** i)
        i -= 1

    return out


def basen2dec(l, basen=2):
    """ Convert base-N number to decimal number """
    n = 0
    k = len(l) - 1
    for i in l:
        n += i * (basen ** k)
        k -= 1
    return n


def dectobase60(n):
    """ Convert decimal number to base-60 number """
    return dec2basen(n, 60)


def base60todec(l):
    """ Convert base-60 number to decimal number """
    return basen2dec(l, 60)


def main(argc, argv):
    if argc != 3:
        print("Usage: %s <N> <basen>" % argv[0], file=sys.stderr)
        print("e.g.", file=sys.stderr)
        print("       %s 128 60" % argv[0], file=sys.stderr)
        return 1

    n = int(argv[1])
    b = int(argv[2])
    lb = dec2basen(n, b)
    print("%d ==> %s" % (n, str(lb)))
    m = basen2dec(lb, b)
    if m == n:
        print("+OK")
    else:
        print("-FAIL")
        return 1

    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
