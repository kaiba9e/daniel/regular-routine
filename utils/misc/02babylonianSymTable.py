#!/usr/bin/python3
""" Create the digital symbol table for ancient Babylonian """

import sys

BABYLON_NS_KEY = 'K'
BABYLON_NS_FISH_BONE = 'B'


def create_ancient_babylonian_num_sym_tbl():
    sym_table = []
    for i in range(1, 60, 1):
        n = i
        na = n // 10  # get the number of keys while key = 1
        nb = n % 10   # get the number of fish bones while fish bone = 10
        sym = BABYLON_NS_FISH_BONE * na + BABYLON_NS_KEY * nb
        sym_table.append({'b10': n, 'b60': sym})
    return sym_table


def main(argc, argv):
    sym_tbl = create_ancient_babylonian_num_sym_tbl()
    for sym in sym_tbl:
        print(sym)

    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
