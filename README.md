# Regular Routine (RR)

We believe that the secret of longevity is the regular and healthy eating and
sleeping habits.

Hence, we record work and rest schedule of Daniel to help him to develop good
habits.

---
* [每天睡足9小时记录单](doc/record-sheet-9H-v1.1.pdf)`v1.1`
* [班主任2021年12月24日的反馈](images/001.20211214.png)
---
> **习惯是一种顽强而巨大的力量，它可以主宰人的一生。因此，人从幼年起就应当通过教育培养一种良好的习惯。**
> ——`培根`
---
> **什么是教育？简单一句话，就是养成良好的习惯。**
> ——`叶圣陶`
---
> **We can do anything we want to if we stick to it long enough.
> 只要我们坚持得足够久，我们就能够做成任何我们想做的事情。**
> ——`Helen Keller`
---
